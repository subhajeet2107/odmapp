-- phpMyAdmin SQL Dump
-- version 3.5.2.2
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jul 01, 2014 at 03:04 PM
-- Server version: 5.5.27
-- PHP Version: 5.4.7

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `odmdb`
--

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE IF NOT EXISTS `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2014_06_30_171608_create_user_table', 1),
('2014_06_30_182840_update_user_table', 2),
('2014_06_30_195739_create_sheets_table', 3),
('2014_07_01_073911_create_products_table', 4),
('2014_07_01_101712_add_size_column', 5);

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE IF NOT EXISTS `products` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `category` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `brand` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `model` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `tonnage` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `starmodel` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `segment` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `mrp` int(11) NOT NULL,
  `dp` int(11) NOT NULL,
  `srp` int(11) NOT NULL,
  `sheet_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `sheet_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `size` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `products_sheet_id_foreign` (`sheet_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=19 ;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `category`, `brand`, `model`, `code`, `tonnage`, `starmodel`, `segment`, `mrp`, `dp`, `srp`, `sheet_name`, `sheet_id`, `created_at`, `updated_at`, `size`) VALUES
(1, 'AC', 'Panasonic', '102PY', '4011136', '0.75', '2', '', 19990, 19940, 0, 'data-3', 3, '2014-07-01 04:01:33', '2014-07-01 04:01:33', ''),
(2, 'AC', 'Panasonic', '122LY', '4011138', '1', '2', '', 22390, 22340, 0, 'data-3', 3, '2014-07-01 04:01:33', '2014-07-01 04:01:33', ''),
(3, 'AC', 'Panasonic', '123LY', '4011139', '1', '3', '', 24140, 24840, 0, 'data-3', 3, '2014-07-01 04:01:33', '2014-07-01 04:01:33', ''),
(4, 'AC', 'Panasonic', '102PY', '4011136', '0.75', '2', '', 19990, 19940, 0, 'data-3', 3, '2014-07-01 04:49:58', '2014-07-01 04:49:58', ''),
(5, 'AC', 'Panasonic', '122LY', '4011138', '1', '2', '', 22390, 22340, 0, 'data-3', 3, '2014-07-01 04:49:58', '2014-07-01 04:49:58', ''),
(6, 'AC', 'Panasonic', '123LY', '4011139', '1', '3', '', 24140, 24840, 0, 'data-3', 3, '2014-07-01 04:49:58', '2014-07-01 04:49:58', ''),
(7, 'MVO Convection', '', 'MC28H5135VK/TL', '', '', '', '', 17400, 15900, 15900, 'data-1', 1, '2014-07-01 04:49:58', '2014-07-01 04:49:58', ''),
(8, 'Digital Invertor', '', 'AR12HV5NBWK', '', '', '', '', 41600, 37500, 37500, 'data-1', 1, '2014-07-01 04:49:58', '2014-07-01 04:49:58', ''),
(9, 'WM Front Loader Fully Automatic', '', 'WF1600NCW/TL', '', '', '', '', 26990, 24990, 25000, 'data-1', 1, '2014-07-01 04:49:58', '2014-07-01 04:49:58', ''),
(10, 'AC', 'Panasonic', '102PY', '4011136', '0.75', '2', '', 19990, 19940, 0, 'data-3', 3, '2014-07-01 04:51:16', '2014-07-01 04:51:16', ''),
(11, 'AC', 'Panasonic', '122LY', '4011138', '1', '2', '', 22390, 22340, 0, 'data-3', 3, '2014-07-01 04:51:16', '2014-07-01 04:51:16', ''),
(12, 'AC', 'Panasonic', '123LY', '4011139', '1', '3', '', 24140, 24840, 0, 'data-3', 3, '2014-07-01 04:51:16', '2014-07-01 04:51:16', ''),
(13, 'FPD', 'PHILIPS', '20PFL3738', '', '', '', 'LED', 11200, 11110, 10890, 'data-2', 2, '2014-07-01 04:57:18', '2014-07-01 04:57:18', '20'),
(14, 'FPD', 'PHILIPS', '20PFL3938', '', '', '3', 'LED', 12000, 11110, 10890, 'data-2', 2, '2014-07-01 04:57:38', '2014-07-01 04:57:38', '20'),
(16, 'AB', 'PHILIPS', '23HK', '4011345', '2', '2', '', 32004, 30100, 31120, 'data-2', 3, '2014-07-01 06:38:00', '2014-07-01 06:38:00', ''),
(18, 'AC', 'PHILIPS', '23HJ', '', '', '', '', 32003, 30100, 31119, 'data-3', 3, '2014-07-01 07:32:46', '2014-07-01 07:32:46', '');

-- --------------------------------------------------------

--
-- Table structure for table `sheets`
--

CREATE TABLE IF NOT EXISTS `sheets` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;

--
-- Dumping data for table `sheets`
--

INSERT INTO `sheets` (`id`, `name`, `description`, `created_at`, `updated_at`) VALUES
(1, 'data-1', 'Convection Oven and Washing Machine', '2014-07-01 03:18:21', '2014-07-01 03:18:21'),
(2, 'data-2', 'Philips LED Tvs', '2014-07-01 03:18:21', '2014-07-01 03:18:21'),
(3, 'data-3', 'Panasonic ACs', '2014-07-01 03:18:21', '2014-07-01 03:18:21');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `remember_token` varchar(255) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `admin` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `username`, `password`, `email`, `remember_token`, `created_at`, `updated_at`, `admin`) VALUES
(1, 'admin', '$2y$10$lg7UNwkbx0sj0IOTt7iSdeoqD3Re9gvYyEgzuULwOKzs70sREd1GS', 'dwij.stardust@gmail.com', 'BUwdrQ9hEIZ7geYV0qe3Xg9F1z7T6Z7CkoEeiiyFnJWQWwSyrlzW0sQLUd1B', '2014-06-30 12:07:56', '2014-07-01 01:34:07', 1);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `products`
--
ALTER TABLE `products`
  ADD CONSTRAINT `products_sheet_id_foreign` FOREIGN KEY (`sheet_id`) REFERENCES `sheets` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
